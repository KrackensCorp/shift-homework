package com.meteo

import com.meteo.ext.print
import java.lang.Exception

fun main() {
    try {
        getRequestMeteoStation {
            getCity()
            getTemperature()
        }.print()
    } catch (exp: Exception) {
        println(exp.message?.toUpperCase())
    }
}