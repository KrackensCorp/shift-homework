package com.meteo

fun getRequestMeteoStation(meteo: MeteoStation.() -> Unit) =
    MeteoStation().apply(meteo).getRequest()

class MeteoStation {
    private var city: String? = null
    private var temperature: String? = null

    fun getCity() {
        city = safeReadLine(
            message = "Привет! В каком городе ты находишься?",
            errorMessage = "Введите название города"
        )
    }

    fun getTemperature() {
        temperature = safeReadLine(
            message = "Какая там температура?",
            errorMessage = "Введите температуру в виде числа"
        )
    }

    fun getRequest(): String {
        temperature?.let { temperature ->
            city?.let { city ->
                return when (temperature.toIntOrNull()) {
                    null -> "Какая-то не понятная температура у вас в ${city}e :/"
                    in -50..14 -> "Ух, у вас в ${city}e холодно!"
                    in 15..24 -> "Нормально у вас в ${city}e..."
                    in 25..50 -> "Ух, у вас в ${city}e жарко!"
                    else -> "Катастрофа у вас в ${city}e, помянем F."
                }
            } ?: return "Ты не указал город >:("
        } ?: return "Ты не сказал мне температуру :("
    }

    @Throws(IllegalStateException::class)
    private fun safeReadLine(message: String, errorMessage: String? = null): String {
        println(message)
        val request = readLine()
        return if (request.isNullOrBlank()) {
            throw IllegalStateException(errorMessage)
        } else {
            request
        }
    }
}